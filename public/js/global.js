$(document).ready(function() {
    document.getElementById("getFile").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("audioContent").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
});


//------------------------------------------------------------------------------
// Speech to Text Service - IBM Bluemix
//------------------------------------------------------------------------------
function convertSpeechToText() {

  var filename = $('#getFile').val().split('\\').pop();
  console.log(filename);

  $.post("/speech-to-text-input", {path:filename}).done(function(data){
      console.log(data);
      //create the tags with the image element
      var audioResult_html = "<p>" + data + "</p>";
      //Print the result in the div
      $("#text-convert").html(audioResult_html);
    });
};
//------------------------------------------------------------------------------


function scroll() {
  $('html, body').animate({
      scrollTop: $($('#second-part')).offset().top
  }, 500);
  return false;
}



$(document).ready(function() {

  var chart_container = $('#second-part');
  chart_container.hide();
  $('#text-convert').hide();
  $('#bt-tone-analyzer').click(function() {
    chart_container.show();
    scroll();
  });
  $('#bt-convert-speech').click(function(){
      $('#text-convert').slideToggle("slow",function(){

      });
  });

})


//------------------------------------------------------------------------------
// Tone Analyzer Service - IBM Bluemix
//------------------------------------------------------------------------------
function toneAnalyzer() {
  var filename = $('#getFile').val().split('\\').pop();

    //Send a post request to the server to get the image from the input text from the user
    $.post("/tone-analyzer-input", {path:filename}).done(function(data){
      console.log("test");
      console.log(data);
      //create the tags with the image element
      var textResult_html = "";
      for(var i = 0; i < data.document_tone.tones.length; i++){
        textResult_html += '<ul>';
        textResult_html += "<li>Emotion: " + data.document_tone.tones[i].tone_name + "</li>";
        textResult_html += "<li>Score: " + Math.round(data.document_tone.tones[i].score * 100) + "%" + "</li>";
        textResult_html += '</ul>';
      }
      //console.log(data.document_tone.tones[0].tone_name);
      //Print the result in the div
      $("#toneResult").html(textResult_html);
      createChart(data);
    });




}

function createChart(array){
  var lbl = [];
  var values = [];
  for(var i = 0; i < array.document_tone.tones.length; i++){
    lbl.push(array.document_tone.tones[i].tone_name);
    values.push(Math.round(array.document_tone.tones[i].score * 100));
  }
  var ctx = document.getElementById('myChart').getContext('2d');

  var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'line',
      // The data for our dataset
      data: {
          labels: lbl,
          datasets: [{
              label: "Emotion",
              backgroundColor: 'skyblue',
              borderColor: 'skyblue',
              data: values,
          }]
      },

      // Configuration options go here
      options: {
        responsive:true,
        maintainAspectRatio: false,
      }

  });

}

// Expose globally your audio_context, the recorder instance and audio_stream
var audio_context;
var recorder;
var audio_stream;

/**
 * Patch the APIs for every browser that supports them and check
 * if getUserMedia is supported on the browser.
 *
 */
function Initialize() {
    try {
        // Monkeypatch for AudioContext, getUserMedia and URL
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        window.URL = window.URL || window.webkitURL;

        // Store the instance of AudioContext globally
        audio_context = new AudioContext;
        console.log('Audio context is ready !');
    } catch (e) {
        alert('No web audio support in this browser!');
    }
}

/**
 * Starts the recording process by requesting the access to the microphone.
 * Then, if granted proceed to initialize the library and store the stream.
 *
 * It only stops when the method stopRecording is triggered.
 */
function startRecording() {
    // Access the Microphone using the navigator.getUserMedia method to obtain a stream
    navigator.getUserMedia({ audio: true }, function (stream) {
        // Expose the stream to be accessible globally
        audio_stream = stream;
        // Create the MediaStreamSource for the Recorder library
        var input = audio_context.createMediaStreamSource(stream);
        console.log('Media stream succesfully created');

        // Initialize the Recorder Library
        recorder = new Recorder(input);
        console.log('Recorder initialised');

        // Start recording !
        recorder && recorder.record();
        console.log('Recording...');

        // Disable Record button and enable stop button !
        document.getElementById("start-btn").disabled = true;
        document.getElementById("stop-btn").disabled = false;
    }, function (e) {
        console.error('No live audio input: ' + e);
    });
}

/**
 * Stops the recording process. The method expects a callback as first
 * argument (function) executed once the AudioBlob is generated and it
 * receives the same Blob as first argument. The second argument is
 * optional and specifies the format to export the blob either wav or mp3
 */
function stopRecording(callback, AudioFormat) {
    // Stop the recorder instance
    recorder && recorder.stop();
    console.log('Stopped recording.');

    // Stop the getUserMedia Audio Stream !
    audio_stream.getAudioTracks()[0].stop();

    // Disable Stop button and enable Record button !
    document.getElementById("start-btn").disabled = false;
    document.getElementById("stop-btn").disabled = true;

    // Use the Recorder Library to export the recorder Audio as a .wav file
    // The callback providen in the stop recording method receives the blob
    if(typeof(callback) == "function"){

        /**
         * Export the AudioBLOB using the exportWAV method.
         * Note that this method exports too with mp3 if
         * you provide the second argument of the function
         */
        recorder && recorder.exportWAV(function (blob) {
            callback(blob);

            // create WAV download link using audio data blob
            // createDownloadLink();

            // Clear the Recorder to start again !
            recorder.clear();
        }, (AudioFormat || "audio/wav"));
    }
}

// Initialize everything once the window loads
window.onload = function(){
    // Prepare and check if requirements are filled
    Initialize();

    // Handle on start recording button
    document.getElementById("start-btn").addEventListener("click", function(){
        startRecording();
    }, false);

    // Handle on stop recording button
    document.getElementById("stop-btn").addEventListener("click", function(){
        // Use wav format
        var _AudioFormat = "audio/wav";
        // You can use mp3 to using the correct mimetype
        //var AudioFormat = "audio/mpeg";

        stopRecording(function(AudioBLOB){
            // Note:
            // Use the AudioBLOB for whatever you need, to download
            // directly in the browser, to upload to the server, you name it !

            // In this case we are going to add an Audio item to the list so you
            // can play every stored Audio
            var url = URL.createObjectURL(AudioBLOB);
            var li = document.createElement('li');
            var au = document.createElement('audio');
            var hf = document.createElement('a');

            au.controls = true;
            au.src = url;
            hf.href = url;
            // Important:
            // Change the format of the file according to the mimetype
            // e.g for audio/wav the extension is .wav
            //     for audio/mpeg (mp3) the extension is .mp3
            hf.download = new Date().toISOString() + '.wav';
            hf.innerHTML = hf.download;
            li.appendChild(au);
            li.appendChild(hf);
            recordingslist.appendChild(li);
        }, _AudioFormat);
    }, false);
};
