/*eslint-env node*/

//------------------------------------------------------------------------------
// node.js starter application for Bluemix
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// bodyParser
var bodyParser = require('body-parser');


// fs: FileSystem Access
var fs = require('fs');

// create a new express server
var app = express();

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());

// serve the files out of ./public as our main files
app.use(express.static(__dirname + '/public'));

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

// start server on the specified port and binding host
app.listen(appEnv.port, '0.0.0.0', function() {
  // print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});


app.post("/speech-to-text-input", function (req, res) {
  console.log(req.body);
 	var SpeechToTextV1 =  require('watson-developer-cloud/speech-to-text/v1'); // se crea la instancia del speech-to-text
  var fs = require('fs');
  var speech_to_text = new SpeechToTextV1({ // se definen las credenciales
    username: '45faf9a5-f731-438c-9f84-ac790a2764b4',
    password: 'FQDhJljZN0p3',
    headers: {
      'X-Watson-Learning-Opt-Out': 'true'
    }
  });

  var params = {
    model: 'es-ES_BroadbandModel',
    content_type: 'audio/wav',
    'interim_results': true,
    'max_alternatives': 3,
    audio_file: fs.createReadStream('public/audio/' + req.body.path)
  };

  //Create stream
  var recognizeStream = speech_to_text.createRecognizeStream(params);

  //Pipe in the audio
  fs.createReadStream('public/audio/' + req.body.path).pipe(recognizeStream);
  // Pipe out the transcription to a file.
  recognizeStream.pipe(fs.createWriteStream('transcription.txt'));

  // Get strings instead of buffers from 'data' events.
  recognizeStream.setEncoding('utf8');

  // Listen for events.
  recognizeStream.on('results', function(event) { onEvent('Results:', event); });
  recognizeStream.on('data', function(event) { onEvent('Data:', event); });
  recognizeStream.on('error', function(event) { onEvent('Error:', event); });
  recognizeStream.on('close', function(event) { onEvent('Close:', event); });
  recognizeStream.on('speaker_labels', function(event) { onEvent('Speaker_Labels:', event); });

  // Displays events on the console.
  function onEvent(name, event) {
    if(name === 'Data:'){
      res.json(JSON.stringify(event, null, 2));
    }
    console.log(name, JSON.stringify(event, null, 2));
  };
  });

  app.post("/tone-analyzer-input", function (req, res) {
    var ToneAnalyzerV3 = require('watson-developer-cloud/tone-analyzer/v3');
    var tone_analyzer = new ToneAnalyzerV3({
      username: '2d75e7c9-8677-436c-8b7d-07e489315e00',
      password: 'TqqSVxjfdlMd',
      version_date: '2017-10-30',
      headers: {
        'X-Watson-Learning-Opt-Out': 'true'
      }
    });
  var json = JSON.parse(fs.readFileSync('public/documentos/'+req.body.path, 'utf8')).text;
  var params = {
    // Get the text from the JSON file.
    text: json,
    tones: 'emotion'
  };

  tone_analyzer.tone(params, function(error, response) {
    if (error){
        console.log('error:', error);
    }else{
      res.json(response);
      }
      });
});
